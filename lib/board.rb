class Board
   attr_accessor :grid


   def initialize(grid = [])
     if grid.length != 0
        @grid = grid
     else
       @grid = []
       3.times do |row_index|
           @grid[row_index] = Array.new(3)
        end
     end
   end

   def place_mark(pos,mark)
     @grid[pos[0]][pos[1]] = mark
   end

   def empty?(pos)
     @grid[pos[0]][pos[0]] == nil
   end

   def winner
     return_val = nil
     case
     when ((@grid[0][0] == @grid[0][1]) and (@grid[0][1] == @grid[0][2]) and (@grid[0][0] != nil))
       return_val =  @grid[0][0]
     when ( (@grid[0][0] == @grid[1][1]) and (@grid[1][1] == @grid[2][2]) and (@grid[0][0] != nil))
         return_val =  @grid[0][0]
     when ((@grid[1][0] == @grid[1][1]) and (@grid[1][1] == @grid[1][2]) and (@grid[1][0] != nil))
       return_val =  @grid[1][0]
     when ((@grid[2][0] == @grid[2][1]) and (@grid[2][1] == @grid[2][2]) and (@grid[2][0] != nil))
       return_val =  @grid[2][0]
     when ((@grid[0][2] == @grid[1][1]) and (@grid[1][1] == @grid[2][0]) and (@grid[0][2] != nil))
      return_val =   @grid[0][2]
    when ((@grid[0][2] == @grid[1][2]) and (@grid[1][2] == @grid[2][2]) and (@grid[0][2] != nil))
      return_val =   @grid[0][2]
    when ((@grid[0][0] == @grid[1][0]) and (@grid[1][0] == @grid[2][0]) and (@grid[0][0] != nil))
      return_val =   @grid[0][0]
    when ((@grid[0][1] == @grid[1][1]) and (@grid[1][1] == @grid[2][1]) and (@grid[0][1] != nil))
      return_val =   @grid[0][1]
    end
    return_val
   end


   def over?
     return true if winner != nil
     return false if @grid.flatten.compact == []
      @grid.each_with_index do |col,r_y|
          col.each_with_index do |row,r_x|
            if @grid[r_x][r_y] == nil
             return  false
            end
          end
      end
      true
   end



end

# p n_board.over?
# n_board = Board.new
